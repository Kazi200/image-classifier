
import matplotlib.pyplot as plt
import os
import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torchvision import datasets, transforms, models
import time
import numpy as np
import json
from PIL import Image
from collections import OrderedDict
import argparse

########################################################################################################

parser = argparse.ArgumentParser()
 
parser.add_argument('image_path', action="store")
parser.add_argument('checkpoint', action="store")
parser.add_argument('--gpu', action="store_true", default=False)
parser.add_argument('--category_names ', action="store", dest="category_names", default='cat_to_name.json')
parser.add_argument('--top_k', action="store", dest="top_k", type=int, default=5)

args = parser.parse_args()

########################################################################################################

def process_image(image):
    ''' Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    '''

    normalize = transforms.Normalize(
       mean=[0.485, 0.456, 0.406],
       std=[0.229, 0.224, 0.225]
    )
    preprocess = transforms.Compose([
       transforms.Resize(256),
       transforms.CenterCrop(224),
       transforms.ToTensor(),
       normalize
    ])
    image = Image.open(image)
    img_tensor = preprocess(image)
    img_tensor.unsqueeze_(0)
    img_numpy = img_tensor.numpy()
    img_numpy = img_numpy.reshape(3,224,224)
    
    return img_numpy

########################################################################################################

def predict(image_path, model, topk=args.top_k):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''

    image = torch.from_numpy(process_image(image_path))

    model.eval()
    with torch.no_grad():
        image.unsqueeze_(0)
        logps = model.forward(image)
    
        ps = torch.exp(logps)
        
        probs, classes = ps.topk(topk, dim=1)
        probs = probs.numpy()[0]
        classes = classes.numpy()[0]

    return probs, classes

########################################################################################################

checkpoint = torch.load('./' + args.checkpoint)

if checkpoint['architecture'] == "vgg":
    model = models.vgg16(pretrained=True)
elif checkpoint['architecture'] == "alexnet":
    model = models.alexnet(pretrained=True)
elif checkpoint['architecture'] == "densenet":
    model = models.densenet161(pretrained=True)    

 
for param in model.parameters():
    param.requires_grad = False

classifier = nn.Sequential(OrderedDict([
    ('fc1', nn.Linear(checkpoint['input_layer'], checkpoint['hidden_layer1'])),
    ('relu1', nn.ReLU()),
    ('drop1', nn.Dropout(checkpoint['dropout_prob'])),
    ('fc2', nn.Linear(checkpoint['hidden_layer1'], checkpoint['hidden_layer2'])),
    ('relu2', nn.ReLU()),
    ('drop2', nn.Dropout(checkpoint['dropout_prob'])),
    ('fc3', nn.Linear(checkpoint['hidden_layer2'], checkpoint['output_size'])),
    ('output', nn.LogSoftmax(dim=1))
]))

model.classifier = classifier
model.load_state_dict(checkpoint['state_dict'])
model.class_to_idx = checkpoint['class_to_idx']

if torch.cuda.is_available() and args.gpu:
    device = 'cuda'
else:
    device = 'cpu'

model.to(device)


########################################################################################################

probs, classes = predict('./' + args.image_path, model)


with open('./'+args.category_names, 'r') as f:
    cat_to_name = json.load(f)

inv_d = {v: k for k, v in model.class_to_idx.items()}

named_class = []
for i in classes:
    named_class.append(cat_to_name[inv_d[i]])

print(named_class[0:args.top_k])
print(probs[0:args.top_k])







