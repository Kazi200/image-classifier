# Image-Classifier

Data Scientist - Term 1 - Project 2

## Project Overview
The purpose of the project is to develop an AI application that can distinguish different species of flower through image classification.

This will be acheived through a Deep Learning PyTorch model.

This is comprised of the following:
- pre-trained Neural Network
- image pre-processing
- a feed-forward image classifier

The details of the above are documented in the notebook
