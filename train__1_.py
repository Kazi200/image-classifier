
import matplotlib.pyplot as plt
import os
import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torchvision import datasets, transforms, models
import time
import numpy as np
import json
from PIL import Image
from collections import OrderedDict
import argparse

parser = argparse.ArgumentParser()
 
parser.add_argument('data_dir', action="store")
parser.add_argument('--save_dir', action="store", dest="save_dir",default='')
parser.add_argument('--gpu', action="store_true", default=False)
parser.add_argument('--arch', action="store", dest="arch", default='vgg', help = 'Choose a model architecture: [vgg, alexnet, densenet]')
parser.add_argument('--epochs', action="store", dest="epochs", type=int, default=20)
parser.add_argument('--learning_rate', action="store", dest="learning_rate", type=float, default=0.01)
parser.add_argument('--hidden_units', action="store", dest="hidden_units", type=int, default=1000)

args = parser.parse_args()

train_dir = './' + args.data_dir + '/train/'
valid_dir = './' + args.data_dir + '/valid/'
test_dir = './' + args.data_dir + '/test/'

train_transforms = transforms.Compose([transforms.RandomRotation(30),
                                       transforms.RandomResizedCrop(224),
                                       transforms.RandomHorizontalFlip(),
                                       transforms.ToTensor(),
                                       transforms.Normalize([0.485, 0.456, 0.406],
                                                            [0.229, 0.224, 0.225])])

test_valid_transforms = transforms.Compose([transforms.Resize(255),
                                              transforms.CenterCrop(224),
                                              transforms.ToTensor(),
                                              transforms.Normalize([0.485, 0.456, 0.406],
                                                                   [0.229, 0.224, 0.225])])


train_data = datasets.ImageFolder(train_dir, transform=train_transforms)
test_data = datasets.ImageFolder(test_dir, transform=test_valid_transforms)
valid_data = datasets.ImageFolder(valid_dir, transform=test_valid_transforms)

trainloader = torch.utils.data.DataLoader(train_data, batch_size=64, shuffle=True)
testloader = torch.utils.data.DataLoader(test_data, batch_size=64)
validloader = torch.utils.data.DataLoader(valid_data, batch_size=64)


if args.arch == "vgg":
    model = models.vgg16(pretrained=True)
    number_input_features = model.classifier[0].in_features
elif args.arch == "alexnet":
    model = models.alexnet(pretrained=True)
    number_input_features = model.classifier[1].in_features
elif args.arch == "densenet":
    model = models.densenet161(pretrained=True)   
    number_input_features = model.classifier.in_features

else:
    print("That model is not available in this program")

for param in model.parameters():
    param.requires_grad = False
    
input_layer = number_input_features
hidden_layer1 = 4096
hidden_layer2 = args.hidden_units
output_size = 102
dropout_prob = 0.5
    
classifier = nn.Sequential(OrderedDict([
                          ('fc1', nn.Linear(number_input_features, hidden_layer1)),
                          ('relu1', nn.ReLU()),
                          ('drop1', nn.Dropout(dropout_prob)),
                          ('fc2', nn.Linear(hidden_layer1, args.hidden_units)),
                          ('relu2', nn.ReLU()),
                          ('drop2', nn.Dropout(dropout_prob)),
                          ('fc3', nn.Linear(args.hidden_units, output_size)),
                          ('output', nn.LogSoftmax(dim=1))
                          ]))    
model.classifier = classifier


criterion = nn.NLLLoss()

optimizer = optim.Adam(model.classifier.parameters(), lr=args.learning_rate)

if torch.cuda.is_available() and args.gpu:
    device = 'cuda'
else:
    device = 'cpu'

        
steps = 0
running_loss = 0
print_every = 5

model.to(device)

for epoch in range(args.epochs):
    for inputs, labels in trainloader:
        steps += 1
        inputs, labels = inputs.to(device), labels.to(device)
        
        optimizer.zero_grad()
        
        logps = model.forward(inputs)
        loss = criterion(logps, labels)
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        
        if steps % print_every == 0:
            valid_loss = 0
            accuracy = 0
            model.eval()
            with torch.no_grad():
                for inputs, labels in validloader:
                    inputs, labels = inputs.to(device), labels.to(device)
                    logps = model.forward(inputs)
                    batch_loss = criterion(logps, labels)
                    
                    valid_loss += batch_loss.item()
                    
                    # Calculate accuracy
                    ps = torch.exp(logps)
                    top_p, top_class = ps.topk(1, dim=1)
                    equals = top_class == labels.view(*top_class.shape)
                    accuracy += torch.mean(equals.type(torch.FloatTensor)).item()
                    
            print(f"Epoch {epoch+1}/{args.epochs}.. "
                  f"Train loss: {running_loss/print_every:.3f}.. "
                  f"Validation loss: {valid_loss/len(validloader):.3f}.. "
                  f"Validation accuracy: {accuracy/len(validloader):.3f}")
            running_loss = 0
            model.train()


if not os.path.exists('./' + args.save_dir):
    os.makedirs('./' + args.save_dir)
else:
    pass


model.class_to_idx = train_data.class_to_idx

checkpoint = {'input_layer': input_layer,
                'output_size': output_size,
                'hidden_layer1': hidden_layer1,
                'hidden_layer2': args.hidden_units,
                'dropout_prob': dropout_prob,
                'state_dict': model.state_dict(),
                'class_to_idx': model.class_to_idx,
                'architecture': args.arch             
             }

torch.save(checkpoint, './' + args.save_dir + '/checkpoint.pth')
